import { Component, OnInit } from '@angular/core';
import { UserPhoto } from '../models/photo.interface';
import { PhotoService } from '../services/photo.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  public photos: UserPhoto[] = [];

  constructor(private photoService: PhotoService) {
  }

  async ngOnInit(): Promise<void> {
    await this.photoService.loadSaved().then(() => {
      this.photos = this.photoService.getPhotos();
    });
  }

  newPhoto(): void {
    this.photoService.addNewToGallery();
  }
}
